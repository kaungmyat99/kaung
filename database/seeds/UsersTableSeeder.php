<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [[
            'id'             => 1,
            'student_id'     =>1,   
            'name'           => 'Admin',
            'gender'         =>'male',
            'phone'          =>'09785412',
            'address'        =>'pinlon',
            'current_address'=>'mdy',
            'image'          =>'/images/readbook.png',
            'facebook'       =>'www.facebook.com',
            'kingqueenselected'         =>'yes',
            'email'          => 'admin123@admin123.com',
            'password'       => '$2y$10$imU.Hdz7VauIT3LIMCMbsOXvaaTQg6luVqkhfkBcsUd.SJW2XSRKO',
            'remember_token' => null,
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
            'deleted_at'     => null,
        ],
        [
            'id'             => 2,
            'student_id'     =>2,   
            'name'           => 'Kaung Myat Moe',
            'gender'         =>'male',
            'phone'          =>'09785412',
            'address'        =>'pinlon',
            'current_address'=>'mdy',
            'image'          =>'/images/readbook.png',
            'facebook'       =>'www.facebook.com',
            'kingqueenselected'         =>'yes',
            'email'          => 'adminadmin00@admin.com',
            'password'       => '$2y$10$imU.Hdz7VauIT3LIMCMbsOXvaaTQg6luVqkhfkBcsUd.SJW2XSRKO',
            'remember_token' => null,
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
            'deleted_at'     => null,
        ],[
            'id'             => 3,
            'student_id'     =>3,   
            'name'           => 'Aung Htet Win',
            'gender'         =>'male',
            'phone'          =>'09785412',
            'address'        =>'pinlon',
            'current_address'=>'mdy',
            'image'          =>'/images/readbook.png',
            'facebook'       =>'www.facebook.com',
            'kingqueenselected'         =>'yes',
            'email'          => 'adminadminkmm@admin.com',
            'password'       => 'password',
            'remember_token' => null,
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
            'deleted_at'     => null,
        ]
    ];

        User::insert($users);
    }
}
