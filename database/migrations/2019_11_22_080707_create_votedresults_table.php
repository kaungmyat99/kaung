<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotedresultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votedresults', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('finalkingqueen_id');
            $table->foreign('finalkingqueen_id')->references('id')->on('kingqueens');
            $table->integer('count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votedresults');
    }
}
