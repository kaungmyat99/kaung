<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKingqueensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kingqueens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('year_id');
            $table->foreign('year_id')->references('id')->on('years');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('type');
            $table->foreign('type')->references('id')->on('products');
            $table->string('academic');
            $table->string('roll');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kingqueens');
    }
}
