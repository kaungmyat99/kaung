<?php

namespace App\Http\Requests;

use App\Year;
use Gate;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyYearRequest extends FormRequest
{
    public function authorize()
    {
        return abort_if(Gate::denies('year_delete'), 403, '403 Forbidden') ?? true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:years,id',
        ];
    }
}
