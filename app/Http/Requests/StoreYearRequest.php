<?php

namespace App\Http\Requests;

use App\Year;
use Illuminate\Foundation\Http\FormRequest;

class StoreYearRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('year_create');
    }

    public function rules()
    {
        return [
            'start_year' => [
                'required',
            ],
            'end_year' => [
                'required',
            ],
        ];
    }
}
