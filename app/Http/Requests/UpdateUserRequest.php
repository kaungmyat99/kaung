<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('user_edit');
    }

    public function rules()
    {
        return [
            'student_id'     => [
                'required','digits_between:3,10',
            ],
            'name'     => [
                'required','regex:/^[a-zA-Z ]+$/',
            ],
            'gender'     => [
                'required',
            ],
            'phone'     => [
                'required','min:11','numeric',
            ],
            'address'     => [
                'required',
            ],
            'current_address'     => [
                'required',
            ],
             
            'facebook'     => [
                'required','regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            ],
            'kingqueenselected'     => [
                'required',
            ],
            'email'    => [
                'required','email',
            ],
             
            'roles.*'  => [
                'integer',
            ],
            'roles'    => [
                'required',
                'array',
            ],
        ];
    }
}
