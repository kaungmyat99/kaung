<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('user_create');
    }

    public function rules()
    {
        return [
            'student_id'     => [
                'required','digits_between:3,10',
            ],
            'name'     => [
                'required','regex:/^[a-zA-Z ]+$/',
            ],
            'gender'     => [
                'required',
            ],
            'phone'     => [
                'required','min:11','numeric',
            ],
            'address'     => [
                'required',
            ],
            'current_address'     => [
                'required',
            ],
            'image'     => [
                'required','mimes:jpeg,jpg,png,gif|required|max:10000',
            ],
            'facebook'     => [
                'required','regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            ],
            'kingqueenselected'     => [
                'required',
            ],
            'email'    => [
                'required','email','unique:users',
            ],
            'password' => [
                'required','min:8','max:20',
            ],
            'roles.*'  => [
                'integer',
            ],
            'roles'    => [
                'required',
                'array',
            ],
        ];
    }
}
