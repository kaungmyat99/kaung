<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Role;
use App\User;
use App\Kingqueen;
use App\Product;
use DB;

class KingQueenController extends Controller
{
    //

     public function index( )
    {
        abort_unless(\Gate::allows('user_access'), 403);
          $query = 'select kingqueens.id,users.image,users.Name,kingqueens.roll,products.name from products join users join kingqueens on kingqueens.user_id=users.id and kingqueens.type=products.id';

          $kingqueen=\DB::select($query);
          
         return view('admin.kingqueen.index',compact('kingqueen'));
    
   
    }

    public function create( )
    {
        
        
         
        abort_unless(\Gate::allows('user_create'), 403);
         $query="select id,DATE_FORMAT(start_year, '%Y') as start_year,DATE_FORMAT(end_year, '%Y') as end_year from years order by id desc";
          $years=\DB::select($query);
          $query = 'select * from users';
          $users=\DB::select($query);
         $query = "select * from products where status='yes'";
          $products=\DB::select($query);
        
        return view('admin.kingqueen.create', compact('years','users','products' ));
    }

    public function store(Request $request)
    {
       
        abort_unless(\Gate::allows('user_create'), 403);
         
          $kingqueen=new kingqueen();
          $kingqueen->year_id=request('year_id');
          $kingqueen->user_id   =request('user_id');
          $kingqueen->type=request('product_id');
          $kingqueen->academic=request('academic_id');
          $kingqueen->roll=request('roll');
          $kingqueen->save();
       
        return redirect()->route('admin.kingqueen.index');
    }

    public function edit(Kingqueen $kingqueen)
    {

                
          $query="select id,DATE_FORMAT(start_year, '%Y') as start_year,DATE_FORMAT(end_year, '%Y') as end_year from years order by id desc";
          $years=\DB::select($query);
          $query = 'select * from users';
          $users=\DB::select($query);
          $query = "select * from products where status='yes'";
          $products=\DB::select($query);
          $id=$kingqueen->id;
          $year_idselected=$kingqueen->year_id;
          $user_idselected=$kingqueen->user_id;
          $product_idselected=$kingqueen->type;
          $academic_idselected=$kingqueen->academic;
          $roll=$kingqueen->roll;
        return view('admin.kingqueen.edit', compact('kingqueen','years','users', 'products','year_idselected','user_idselected','product_idselected','academic_idselected','roll'));
    }


    public function update(Request $request,Kingqueen $kingqueen )
     {
         
      
         $U_id=$kingqueen->id;
         $U_year_id=$request->year_id;
         $U_user_id=$request->user_id;
         $U_product_id=$request->product_id;
         $U_academic_id=$request->academic_id;
         $U_roll=$request->roll;

           $up=\DB::update('update kingqueens set  year_id = ?,user_id=?,type=?,academic=?,roll=? where id = ?',[$U_year_id,$U_user_id,$U_product_id,$U_academic_id,$U_roll,$U_id]);
         return redirect()->route('admin.kingqueen.index');
    }
  public function show(Kingqueen $kingqueen)
    {
        
           $query = 'select kingqueens.id,users.image,users.Name,kingqueens.roll,products.name from products join users join kingqueens on kingqueens.user_id=users.id and kingqueens.type=products.id where kingqueens.id=?';

           $kingqueeninfo=\DB::select($query,[$kingqueen->id]);

     //  dd($kingqueeninfo);
        return view('admin.kingqueen.show', compact('kingqueeninfo'));
    }
     

    public function destroy(Request $request,Kingqueen $kingqueen)
    {
        
 
       $query = 'delete  from kingqueens where id=?';
       $result=\DB::select($query,[$kingqueen->id]);
       $request->session()->flash('alert-success', 'Delete   successful  !');
 
        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
