<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Votedresult;
use DB;
class VoltedListController extends Controller
{
    //
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('user_access'), 403);

          
            if($request->academic_year==null)
            {
                $academic_yearselected='1';
            }else
            {
                 $academic_yearselected=$request->academic_year; 
            }
             
           if($request->product==null)
            {
               $query = "select id from products where status='yes'";
               $products=\DB::select($query);
             
                $selectedproduct=$products[0]->id;
            }else
            {
                 $selectedproduct=$request->product; 
            }
         $query = "select * from products where status='yes'";
         $products=\DB::select($query);

         $query="select id,DATE_FORMAT(start_year, '%Y') as start_year,DATE_FORMAT(end_year, '%Y') as end_year from years order by id desc";
         $academic_years=\Db::select($query);

         $query = 'select voteds.id,user.name as user_name,king.name as kingqueen,products.name as type from users as king join voteds join users as user join kingqueens join products on kingqueens.user_id=king.id and  voteds.kingqueens_id=kingqueens.id and user.id=voteds.user_id and kingqueens.type=products.id where kingqueens.year_id=? and kingqueens.type=?';
         $users=\DB::select($query,[$academic_yearselected,$selectedproduct]);

         
 
        return view('admin.votedlist.index', compact('users','products','academic_years','academic_yearselected','selectedproduct'));
    }
     public function create(Request $request)
    {
              
              $query="select   kingqueens.id,count(*) as total from voteds join kingqueens join products join users on users.id=kingqueens.user_id and kingqueens.type=products.id and voteds.kingqueens_id=kingqueens.id  group by voteds.kingqueens_id,kingqueens.type order by products.id";
              $count=\DB::select($query);
           for ($i=0; $i <sizeof($count) ; $i++)
             { 
            
                   $votedresult=new votedresult();
                   $votedresult->finalkingqueen_id   =$count[$i]->id;
                   $votedresult->count=$count[$i]->total;
                   $votedresult->save();
              }
 
        return back();
    } 
     public function show( )
    {
        DB::table('voteds')->delete();

        return back();
    }
}
