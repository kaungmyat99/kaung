<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyYearRequest;
use App\Http\Requests\StoreYearRequest;
use App\Http\Requests\UpdateYearRequest;
use App\Year;
class YearsController extends Controller
{
    //
      
     public function index()
    {
      

          $query = "select * from years ";
          $years=\DB::select($query);
        
        return view('admin.years.index', compact('years'));
    }
    public function create()
    {
        
          
        return view('admin.years.create', compact('years'));
    }
    public function store(Request $request)
    {
        $years = Year::create($request->all());
        
        return redirect()->route('admin.years.index');
    }
     public function edit(Year $year)
    {
         return view('admin.years.edit', compact('year'));
    }

    public function update(UpdateYearRequest $request, Year $year)
    {
         $year->update($request->all());

         return redirect()->route('admin.years.index');
    }

    public function show(Year $year)
    {
         return view('admin.years.show', compact('year'));
    }

    
}
