<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Role;
use App\User;
use DB;
class UsersController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('user_access'), 403);

        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('user_create'), 403);

        $roles = Role::all()->pluck('title', 'id');

        return view('admin.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('user_create'), 403);
           if($request->hasfile('image'))
        {
          $img=$request->file('image');
          $new_name=time().'.'.$img->getClientOriginalExtension();
          $img->move(public_path('images'),$new_name);
         } 
         else
         {
              $new_name='sdf';
         }
          $user=new user();
          $user->student_id   =request('student_id');
          $user->name=request('name');
          $user->gender=request('gender');
          $user->phone=request('phone');
          $user->address=request('address');
          $user->current_address=request('current_address');
          $user->image='/images/'.$new_name;
          $user->facebook = request('facebook');
          $user->kingqueenselected=request('kingqueenselected');
          $user->email=request('email');
          $user->password=request('password');
          $user->save();
         
          $user->roles()->sync($request->input('roles', []));
       
        return redirect()->route('admin.users.index');
    }

    public function edit(User $user)
    {
        abort_unless(\Gate::allows('user_edit'), 403);

        $roles = Role::all()->pluck('title', 'id');
        $user->load('roles');
         
        return view('admin.users.edit', compact('roles', 'user'));
    }
    
    public function update(UpdateUserRequest $request, User $user)
    {
        abort_unless(\Gate::allows('user_edit'), 403);

        
         $U_id=$user->id;
         $U_student_id=$request->student_id;
         $U_name=request('name');
         $U_gender = $request->get('gender');
         $U_phone=request('phone');
         $U_address=request('address');
         $U_current_address=$request->current_address;

         
           if($request->hasfile('image'))
              {

                 $image=$request->file('image');
                 $new_name=time().'.'.$image->getClientOriginalExtension();
                 $image->move(public_path('images'),$new_name);
                 $U_image='/images/'.$new_name;
              }
          else
              { 
                   $hashed =DB::table('Users')
                      ->where('id', '=',$user->id)
                      ->value('image');
          
                   $U_image=$hashed;
               }
              
        
         $U_facebook=request('facebook');
         $U_kingqueenselected=request('kingqueenselected');
         $U_email = $request->get('email');
             if(request('password')!=null)
                 {
                       $U_password=request('password');
                       $up=\DB::update('update users set student_id=?,name = ?,gender=?,phone=?,address=?,current_address=?,image=?,facebook=?,kingqueenselected=?,email=?,password=? where id = ?',[$U_student_id,$U_name,$U_gender,$U_phone,$U_address,$U_current_address,$U_image,$U_facebook,$U_kingqueenselected,$U_email,$U_password,$U_id]);
                   
                 }
              else
              {
                       $up=\DB::update('update users set student_id=?,name = ?,gender=?,phone=?,address=?,current_address=?,image=?,facebook=?,kingqueenselected=?,email=?  where id = ?',[$U_student_id,$U_name,$U_gender,$U_phone,$U_address,$U_current_address,$U_image,$U_facebook,$U_kingqueenselected,$U_email,$U_id]);
                  
                  
              }
           
              
         return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        abort_unless(\Gate::allows('user_show'), 403);

        $user->load('roles');

        return view('admin.users.show', compact('user'));
    }

    public function destroy(User $user)
    {
        abort_unless(\Gate::allows('user_delete'), 403);

        $user->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
