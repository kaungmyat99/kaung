<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Role;
use App\User;
use App\Voted;
use DB;
class KingQueenController extends Controller
{
    //
    public function index(Request $request)
    {
    	   
         
             
            $hashed =DB::table('role_user')
                                 ->where('user_id', '=',\Auth::id())
                                 ->value('role_id');
            
         
            if($request->academic_year==null)
            {
                $academic_yearselected='1';
            }else
            {
                 $academic_yearselected=$request->academic_year; 
            }
             
            if($request->product==null)
            {
               $query = "select id from products where status='yes'";
               $products=\DB::select($query);
             
                $selectedproduct=$products[0]->id;
            }else
            {
                 $selectedproduct=$request->product; 
            }
         $query = "select * from products where status='yes'";
         $products=\DB::select($query);
         if(is_null($products))
         {
          dd('dfasfasd');
         }
         $query="select id,DATE_FORMAT(start_year, '%Y') as start_year,DATE_FORMAT(end_year, '%Y') as end_year from years order by start_year desc";
         $academic_years=\Db::select($query);
 
         $query="select * from voteds";
         $query = "select users.id,users.image,users.name,kingqueens.id as ID,kingqueens.type from products join users join kingqueens join years on kingqueens.user_id=users.id and kingqueens.type=products.id and kingqueens.year_id=years.id where  
            years.id=? and products.id=?  ";

          $kingqueens=\DB::select($query,[$academic_yearselected,$selectedproduct]);
         
        
        return view('frontend.main.kingqueen.index',compact('kingqueens', 'academic_years','products','academic_yearselected','selectedproduct','hashed'));
    }
     public function store(Request $request)
    {

          $query="select voteds.user_id,voteds.kingqueens_id,products.id from voteds join kingqueens join products on voteds.kingqueens_id=kingqueens.id and kingqueens.type=products.id";
          $voteds=\DB::select($query);
 
           $query="select id,DATE_FORMAT(start_year, '%Y') as start_year,DATE_FORMAT(end_year, '%Y') as end_year from years order by id desc";
          $years=\DB::select($query);
              
              for ($i=0; $i <sizeof($voteds) ; $i++)
               { 
                      if((\Auth::id()==$voteds[$i]->user_id && $request->kingqueen_id==$voteds[$i]->kingqueens_id) || (\Auth::id()==$voteds[$i]->user_id && $request->type==$voteds[$i]->id))
                       {
                             $request->session()->flash('alert-danger', 'User was already voted!');
                           return back();

                       } 
                }
          $voted=new voted();
          $voted->user_id   =\Auth::id();
          $voted->kingqueens_id=$request->kingqueen_id;
          $voted->save();
          $request->session()->flash('alert-success', 'User was successful voted!');
     
         return back();
    }
    public function show(Request $request)
    {        
            $hashed =DB::table('role_user')
                                 ->where('user_id', '=',\Auth::id())
                                 ->value('role_id');
            $query="select voteds.id,users.name,users.image,products.name as type from voteds join users join  kingqueens join products on voteds.kingqueens_id=kingqueens.id and kingqueens.user_id=users.id and kingqueens.type=products.id where voteds.user_id=?  ";
            $voted=\DB::select($query,[\Auth::id()]);
           
            
           return  view('frontend.main.kingqueen.show',compact('voted','hashed'));
    }
    public function destroy(Request $request)
    {
            
        
        $query = 'delete  from voteds where id=?';
        $result=\DB::select($query,[$request->id]);
          $request->session()->flash('alert-success', 'Delete   successful  !');
 
        return back();
    }

}
