<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Session;
use App\User;
use DB;
class HomeController extends Controller
{
    //
    public function index(Request $request)

    { 
        
             $username = User::find(\Auth::id());
             
             $hashed =DB::table('role_user')
                                 ->where('user_id', '=',\Auth::id())
                                 ->value('role_id');
            $request->session()->flash('alert-success', 'User was successful added!');
              
              
        return view('frontend.home',compact('username','hashed'));
    }
     public function logout()
    { 

        Session::flush();
        Auth::logout();
    
        return view('frontend.home');
    }
    
    
     


 }
