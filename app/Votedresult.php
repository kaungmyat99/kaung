<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Votedresult extends Model
{
    //
     use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'finalkingqueen_id',
        'count',
        'created_at',
        'updated_at',
        'deleted_at',
        'description',
    ];
}
