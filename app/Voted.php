<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voted extends Model
{
    //
     use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'kingqueens_id',
         'created_at',
        'updated_at',
        'deleted_at',
        'description',
    ];
}
