<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Year extends Model
{
    //
      use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'start_year',
        'end_year',
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
        'deleted_at',
         
    ];
}
