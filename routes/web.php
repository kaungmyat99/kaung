<?php

 Route::get('/',function () {
    return view('frontend/home');
});
Route::redirect('/home', '/admin');

Auth::routes(['register' => true]);

    Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');


    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    Route::resource('kingqueen', 'KingQueenController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');


    Route::resource('years', 'YearsController');
    Route::delete('years/destroy', 'YearsController@massDestroy')->name('years.massDestroy');

    Route::resource('votedlist', 'VoltedListController');
    

     Route::resource('votedresult', 'VoltedResultController');
    

});
Route::redirect('/user', '/frontend');
Route::group(['prefix' => 'frontend', 'as' => 'frontend.', 'namespace' => 'Frontend', 'middleware' => ['auth']],  function () {
    
    Route::get('/', 'HomeController@index')->name('home');
     Route::resource('home', 'HomeController') ;
    Route::resource('login', 'LoginController');

    
     Route::resource('users', 'UsersController@index');

    Route::resource('kingqueen', 'KingQueenController');

   
});

