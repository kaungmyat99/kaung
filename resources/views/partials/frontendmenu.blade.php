  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Computer University (Pang long)</a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          
              
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#">Activity</a>
          </li>
       
           
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contact</a>
          </li>
           @unless (!Auth::check())
          <li class="nav-item dropdown mx-0 mx-lg-1">
            <a class="nav-link dropdown-toggle py-3 px-0 px-lg-3" href="#" id="navbardrop" data-toggle="dropdown">
                          Hi,{{ Auth::user()->name }}
               </a>
      <div class="dropdown-menu">
          @if($hashed==1)
        <a class="dropdown-item" href="{{route("admin.home") }}">Dashboard</a>
           @endif
        <a class="dropdown-item" href="{{route("frontend.kingqueen.index")}}">Voting</a>
        <a href="#" class="dropdown-item">Result</a>
        <a href="#" class="dropdown-item">Student Information</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{route("frontend.login.index")}}">Logout </a>
      </div>
    </li>
          
          @endunless
          @unless (Auth::check())
          <li  class="nav-item dropdown mx-0 mx-lg-2">
                    <a href="{{ route('login')}}" class="nav-link dropdown-toggle py-3 px-0 px-lg-3 rounded js-scroll-trigger" data-toggle="dropdown">Menu</a>
                    <div class="dropdown-menu">
                      <a href="{{route("frontend.kingqueen.index")}}" class="dropdown-item">Voting</a>
                      <a href="#" class="dropdown-item">Result</a>
                      <a href="#" class="dropdown-item">Student Information</a>
                      <div class="dropdown-divider"></div>
                      <a href="{{ route('login')}}"class="dropdown-item">Login</a>
                    </div>
            </li>
       @endunless

        </ul>
      </div>
    </div>
  </nav>