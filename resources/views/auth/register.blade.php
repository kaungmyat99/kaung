 @extends('layouts.menu')
 

@section('content')
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
   
        <div class="container-login100" style="background-image: url('/images/bg-01.jpg');">
            <div class="wrap-login100">
                 <form action="{{ route('register') }}" method="post">
    
                       {{ csrf_field() }}
                     

                    <span class="login100-form-title p-b-10 p-t-10">
                        Register
                    </span>
                     <div class="wrap-input100 validate-input" data-validate = "Enter student id">
                        <input class="input100" type="text" name="student_id" placeholder="Student ID">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate = "Enter name">
                        <input class="input100" type="text" name="name" placeholder="Name">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                         <div class="row"  >
                        <div class="col-sm-2 "  ><label for="gender" class="input100">Gender</label> </div>
                    <div class="col-sm-4 " align="center"><label class="input100"><input type="radio"   name="gender" checked value="male">Male</label></div>
                   <div class="col-sm-6 " align="center"> <label class="input100"><input type="radio"   name="gender" value="Female">Female</label> </div>
                 </div>
                   <div class="wrap-input100 validate-input" data-validate = "Enter phone">
                        <input class="input100" type="text" name="phone" placeholder="Phone">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                   <div class="wrap-input100 validate-input" data-validate = "Enter address">
                        <input class="input100" type="text" name="address" placeholder="Address">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                     <div class="wrap-input100 validate-input" data-validate = "Enter current address">
                        <input class="input100" type="text" name="current_address" placeholder="Current Address">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                     <div class="wrap-input100 validate-input" data-validate = "Enter Imgae">
                        <input class="input100" type="file" name="image" placeholder="Image">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                     <div class="wrap-input100 validate-input" data-validate = "Enter facebook">
                        <input class="input100" type="text" name="facebook" placeholder="Facebook">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                       <div class="wrap-input100 validate-input" data-validate = "Enter email">
                        <input class="input100" type="text" name="email" placeholder="Email">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="password" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="password_confirmation" placeholder="Password" required>
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                     
                     
                    <div class="container-login100-form-btn">
                        <input type="submit" name="create" class="login100-form-btn">
                            
                    </div>

                     
                </form>
            </div>
        </div>
    
    

    <div id="dropDownSelect1"></div>
     
<!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="js/main.js"></script>
@endsection