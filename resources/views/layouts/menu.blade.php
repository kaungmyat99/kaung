<!--
=========================================================
* BLK Design System- v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/blk-design-system
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 -->

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="./assetsmenu/img/apple-icon.png">
  <link rel="icon" type="image/png" href="./assetsmenu/img/favicon.png">
  <title>
    Computer University (Pang Long)
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- Nucleo Icons -->
  <link href="/assetsmenu/css/nucleo-icons.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="/assetsmenu/css/blk-design-system.css?v=1.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="/assetsmenu/demo/demo.css" rel="stylesheet" />
</head>

<body class="index-page">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg fixed-top navbar-transparent " color-on-scroll="100">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="https://demos.creative-tim.com/blk-design-system/index.html" rel="tooltip" title="Designed and Coded by Creative Tim" data-placement="bottom" target="_blank">
          <span>Computer University </span>  
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a>
               CUPL
              </a>
            </div>
            <div class="col-6 collapse-close text-right">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <i class="tim-icons icon-simple-remove"></i>
              </button>
            </div>
          </div>
        </div>
        <ul class="navbar-nav">
          <li class="nav-item p-0"  >
            Activity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </li>
          <li class="nav-item p-0"  >
             About&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </li>
          <li class="nav-item p-0"  >
            Content&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </li>
          <li class="dropdown nav-item">
              @unless (!Auth::check())
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <i class="fa fa-cogs d-lg-none d-xl-none"></i> Hi,{{ Auth::user()->name }}
            </a>
            <div class="dropdown-menu dropdown-with-icons">
             
               
                @if($hashed==1)
              <a  href="{{route("admin.home") }}" class="dropdown-item">
                <i class="tim-icons icon-bullet-list-67"></i>Dashboard
              </a>
               @endif
              <a href="{{route("frontend.kingqueen.index")}}" class="dropdown-item">
                <i class="tim-icons icon-image-02"></i>Voting  
              </a>
              <a href="#" class="dropdown-item">
                <i class="tim-icons icon-single-02"></i>Result 
              </a>
               <a href="{{route("frontend.login.index")}}" class="dropdown-item">
                <i class="tim-icons icon-single-02"></i>Logout 
              </a>
               
            </div>
            @endunless
            @unless (Auth::check())
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <i class="fa fa-cogs d-lg-none d-xl-none"></i> Menu
            </a>
            <div class="dropdown-menu dropdown-with-icons">
              <a href="{{route("frontend.kingqueen.index")}}"  class="dropdown-item">
                <i class="tim-icons icon-image-02"></i>Voting  
              </a>
              <a href="#" class="dropdown-item">
                <i class="tim-icons icon-single-02"></i>Result 
              </a>
               <a href="#" class="dropdown-item">
                <i class="tim-icons icon-single-02"></i>Information 
              </a>
                <a href="{{ route('login')}}" class="dropdown-item">
                <i class="tim-icons icon-single-02"></i>Login 
              </a>
               
            </div>
            @endunless
          </li>
           
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar --> 

 <main class="main">


            <div style="padding-top: 100px" class="container-fluid">

                @yield('content')

            </div>


        </main>

      
    <!--   Core JS Files   -->
  <script src="/assetsmenu/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="/assetsmenu/js/core/popper.min.js" type="text/javascript"></script>
  <script src="/assetsmenu/js/core/bootstrap.min.js" type="text/javascript"></script>
  <script src="/assetsmenu/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="/assetsmenu/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="/assetsmenu/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!-- Chart JS -->
  <script src="/assetsmenu/js/plugins/chartjs.min.js"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="/assetsmenu/js/plugins/moment.min.js"></script>
  <script src="/assetsmenu/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!-- Black Dashboard DEMO methods, don't include it in your project! -->
  <script src="/assetsmenu/demo/demo.js"></script>
  <!-- Control Center for Black UI Kit: parallax effects, scripts for the example pages etc -->
  <script src="/assetsmenu/js/blk-design-system.min.js?v=1.0.0" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      blackKit.initDatePicker();
      blackKit.initSliders();
    });

    function scrollToDownload() {

      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }
  </script>
</body>

</html>
