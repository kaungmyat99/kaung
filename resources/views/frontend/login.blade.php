 @extends('layouts.frontend')
 

@section('content')
    <div class="limiter">
        <div class="container-login100" style="background-image: url('/images/bg-01.jpg');">
            <div class="wrap-login100">
                 <form action="{{ route("frontend.users.index") }}" method="get">
    
                       {{ csrf_field() }}
                    <span class="login100-form-logo">
                        <i class="zmdi zmdi-landscape"></i>
                    </span>

                    <span class="login100-form-title p-b-34 p-t-10">
                        Log in
                    </span>

                    <div class="wrap-input100 validate-input" data-validate = "Enter Email">
                        <input class="input100" type="text" name="email" placeholder="Email">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="password" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>

                     
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>

                    <div class="text-center p-t-10">
                        <a class="txt1" href="{{ route("frontend.login.create") }} ">
                            register
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection