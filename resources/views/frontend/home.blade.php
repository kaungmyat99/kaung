 @extends('layouts.menu')
 

@section('content')

<body >

<script type="text/javascript" >
  if(Session::has('jsAlert'))
 alert(session()->get('jsAlert'));
     
</script>


  <!-- Masthead -->
  <br><br>
    <div class=" p-b-100 p-t-200">
  
    <div class="container d-flex align-items-center flex-column">

      <!-- Masthead Avatar Image -->
      <img class="masthead-avatar mb-5" src="/img/cupllogo.jpg" alt="" width="300px" height="300px">
       
 
      <!-- Masthead Heading -->
      <h1 class="masthead-heading text-uppercase mb-0">Computer University</h1>
  
      <!-- Icon Divider -->
      <div class="divider-custom divider-light">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-star"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Masthead Subheading -->
      <p class="masthead-subheading font-weight-light mb-0">Voting - Student Information -Result</p>

    </div>
       </div>  
  
 
@endsection