 @extends('layouts.frontend')
 

@section('content')
    <div class="limiter">
        <div class="container-login100" style="background-image: url('/images/bg-01.jpg');">
            <div class="wrap-login100">
                 <form action="{{ route("frontend.login.store") }}" method="post">
    
                       {{ csrf_field() }}
                     

                    <span class="login100-form-title p-b-10 p-t-10">
                        Register
                    </span>
                     <div class="wrap-input100 validate-input" data-validate = "Enter student id">
                        <input class="input100" type="text" name="student_id" placeholder="Student ID">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate = "Enter name">
                        <input class="input100" type="text" name="name" placeholder="Name">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                         <div class="row"  >
                        <div class="col-sm-2 "  ><label for="gender" class="input100">Gender</label> </div>
                    <div class="col-sm-4 " align="center"><label class="input100"><input type="radio"   name="gender" checked value="male">Male</label></div>
                   <div class="col-sm-6 " align="center"> <label class="input100"><input type="radio"   name="gender" value="Female">Female</label> </div>
                 </div>
                   <div class="wrap-input100 validate-input" data-validate = "Enter phone">
                        <input class="input100" type="text" name="phone" placeholder="Phone">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                   <div class="wrap-input100 validate-input" data-validate = "Enter address">
                        <input class="input100" type="text" name="address" placeholder="Address">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                     <div class="wrap-input100 validate-input" data-validate = "Enter current address">
                        <input class="input100" type="text" name="current_address" placeholder="Current Address">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                     <div class="wrap-input100 validate-input" data-validate = "Enter Imgae">
                        <input class="input100" type="file" name="image" placeholder="Image">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                     <div class="wrap-input100 validate-input" data-validate = "Enter facebook">
                        <input class="input100" type="text" name="facebook" placeholder="Facebook">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                       <div class="wrap-input100 validate-input" data-validate = "Enter email">
                        <input class="input100" type="text" name="email" placeholder="Email">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="password" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>

                     
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>

                     
                </form>
            </div>
        </div>
    </div>
    

    <div id="dropDownSelect1"></div>
     
@endsection