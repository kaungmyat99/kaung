 @extends('layouts.menu')
 

@section('content')

<body id="page-top">

  <!-- Navigation -->
 
  <!-- Masthead -->
  
 <!-- Portfolio Section -->
  <div class=" p-b-34 p-t-100">
   <div class="card">
    <div class="card-header">
      
        <form  action="{{ route("frontend.kingqueen.index") }}" method="get"> 
               <div class="row">
                 <div class="col-sm-1" align="center">
               <label for="name">Academic*</label></div>
              <div class="col-sm-3" align="center" >
       <select   class="form-control" id="sel1" name="academic_year"  >
         
        @foreach($academic_years as $key => $academic_year)
                    <option value="{{ $academic_year->id  }}" {{($academic_year->id==$academic_yearselected)?'selected':''}}>{{$academic_year->start_year}} - {{$academic_year->end_year}} </option>  
                    @endforeach
      </select>  
    </div>
                
         
             <div class="col-sm-1" align="center">
               <label for="name">Type*</label></div>
              <div class="col-sm-3" align="center" >
       <select   class="form-control" id="sel1" name="product"  >
         
        @foreach($products as $key => $product)
                    <option value="{{ $product->id  }}" {{($product->id ==$selectedproduct)?'selected':''}}>{{$product->name}}</option>  
                    @endforeach
      </select>  
    </div>   
     
 <div class="col-sm-2" align="center">
   <button  type="submit" class="btn btn-success">search</button>
 </div>
 <div class="col-sm-2" align="center">
  <a class="btn btn-success"  href="{{ route("frontend.kingqueen.show",$selectedproduct) }}">voted List</a>
 </div>
</div>
</form>
 </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
               <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> 
                <tbody>
                             
                             <div class="row">
                               @foreach($kingqueens as $key => $kingqueen)
                               <form  action="{{ route("frontend.kingqueen.store") }}" method="POST" enctype="multipart/form-data">
                               @csrf
                               <div class="col-md-3">
                                
                            <div align="center" data-entry-id="{{ $kingqueen->id }}">
                                  <input type="hidden" name="id" value="{{ $kingqueen->id }}">
                                  <input type="hidden" name="type" value="{{ $kingqueen->type }}">
                                  <input type="hidden" name="kingqueen_id" value="{{ $kingqueen->ID }}">
                                  <img class="masthead-avatar mb-5" src=" {{ $kingqueen->image ?? '' }}" alt="" width="200px" height="200px" > <br>
                                  <span>{{ $kingqueen->name }} </span><br>
                                <input type="submit"  class="btn btn-xs btn-primary"   value=" {{ trans('global.king_queen.fields.vote') }}" >
                                     
                             
                          </div>
                        </div> 
                      </form>
                            @endforeach
                        
                            </div>

                         
                    
                </tbody>
            </table>
        </div>
    </div>

</div>
 </div>

@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('user_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
 

    

 
  
      
@endsection