 @extends('layouts.menu')
 

@section('content')

<body id="page-top">

  <!-- Navigation -->

 
  <!-- Masthead -->
  
 <!-- Portfolio Section -->
  <div class=" p-b-34 p-t-100">
<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>

    <div class="card-body">
        
                               
        <table class="table table-bordered table-striped">
            <tbody>
                <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
                <tr>
                     <th>Image</th>
                     <th>Name</th>
                     <th>Type</th>
                     <th>Action</th>
                </tr>
                @foreach($voted as $key => $voted)
                <tr>
                   
                    <td> <img src=" {{ $voted->image ?? '' }}" alt="" width="80px" height="80px" ></td>
                    <td>{{$voted->name}}</td>
                    <td>{{$voted->type}}</td>
                    <td>
                         
                                     <form action="{{ route('frontend.kingqueen.destroy', $voted->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                         <input type="hidden" name="id" value="{{$voted->id}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                 
                    </td>
                    
                 </tr>
                 @endforeach
            </tbody>
        </table>
     
    </div>
</div>
</div>
</body>

@endsection