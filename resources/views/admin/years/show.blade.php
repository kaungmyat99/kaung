@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.year.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.year.fields.start_year') }}
                    </th>
                    <td>
                        {{ $year->start_year }}
                    </td>
                </tr>

                <tr>
                    <th>
                        {{ trans('global.year.fields.end_year') }}
                    </th>
                    <td>
                        {{ $year->end_year }}
                    </td>
                </tr>
                <tr>
                     <tr>
                    <th>
                        {{ trans('global.year.fields.start_date') }}
                    </th>
                    <td>
                        {{ $year->start_date }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.year.fields.end_date') }}
                    </th>
                    <td>
                        {{ $year->end_date }}
                    </td>
                </tr>
                 
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection