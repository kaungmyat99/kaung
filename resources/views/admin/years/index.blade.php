@extends('layouts.admin')
@section('content')
@can('year_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.years.create") }}">
                {{ trans('global.add') }} {{ trans('global.year.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('global.year.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
          
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">
  
                        </th>
                        <th>
                            {{ trans('global.year.fields.start_year') }}
                        </th>
                        <th>
                            {{ trans('global.year.fields.end_year') }}
                        </th>
                         <th>
                            {{ trans('global.year.fields.start_date') }}
                        </th>
                        <th>
                            {{ trans('global.year.fields.end_date') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
               <tbody>
                    @foreach($years as $key => $year)
                        <tr data-entry-id="{{ $year->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $year->start_year ?? '' }}
                            </td>
                            <td>
                                {{ $year->end_year ?? '' }}
                            </td>
                             <td>
                                {{ $year->start_date ?? '' }}
                            </td>
                             <td>
                                {{ $year->end_date ?? '' }}
                            </td>
                            
                            
                            <td>
                                @can('year_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.years.show', $year->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('year_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.years.edit', $year->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.years.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('role_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection