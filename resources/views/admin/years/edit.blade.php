@extends('layouts.admin')
@section('content')
 <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css"></link>
   
    <style>
    .ui-datepicker-calendar {
        display: none;
    }
    </style>
<div class="card">
    <div class="card-header">
       {{ trans('global.edit') }} {{ trans('global.year.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.years.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                <div class="row">
                <div class="col-sm-4 " align="center">
                <label for="Strat Date">{{ trans('global.year.fields.start_year') }}*</label></div>
                <div class="col-sm-6 " align="center"><input type="date" id="start_year" name="start_year" class="form-control" value="{{ old('start_year', isset($year) ? $year->start_year : '') }}">
                @if($errors->has('start_year'))
                    <em class="invalid-feedback">
                        {{ $errors->first('start_year') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.year.fields.start_year_helper') }}
                </p>
            </div></div></div>
               <div class="form-group {{ $errors->has('end_year') ? 'has-error' : '' }}">
               <div class="row">
                <div class="col-sm-4 " align="center">
                 <label for="Strat Date">{{ trans('global.year.fields.end_year') }}*</label></div>
                <div class="col-sm-6 " align="center"><input type="date" id="end_year" name="end_year" class="form-control" value="{{ old('end_year', isset($year) ? $year->end_year : '') }}">
                @if($errors->has('end_year'))
                    <em class="invalid-feedback">
                        {{ $errors->first('end_year') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.year.fields.end_year_helper') }}
                </p>
            </div></div></div>
              <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
               <div class="row">
                <div class="col-sm-4 " align="center">
                 <label for="Strat Date">{{ trans('global.year.fields.start_date') }}*</label></div>
                 <div class="col-sm-6 " align="center">
                <div class="controls input-append date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                    <input size="16" type="text" name="start_date" value="{{ old('start_date', isset($year) ? $year->start_date : '') }}" id="start_date"readonly>
                    <span class="add-on"><i class="icon-remove"></i></span>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
                <input type="hidden" id="dtp_input1" value="" /><br/>
            </div>
        </div>
    </div>
            <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                <div class="row">
                <div class="col-sm-4 " align="center">
                    <label for="Strat Date">{{ trans('global.year.fields.end_date') }}*</label></div>
              
                
                <div class="controls input-append date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                   <div class="col-sm-6 " align="center">  <input  type="text"   name="end_date"  value="{{ old('end_date', isset($year) ? $year->end_date : '') }}" id="end_date" readonly>
                    <span class="add-on"><i class="icon-remove"></i></span>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
                <input type="hidden" id="dtp_input1" value="" /><br/>
            </div>
        </div>
    </div>
                <div align="center">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>  
        </form>
    </div>
</div>
 
 
 

     
@endsection