@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.user.fields.student_id') }}
                    </th>
                    <td>
                        {{ $user->student_id }}
                    </td>
                </tr>

                <tr>
                    <th>
                        {{ trans('global.user.fields.name') }}
                    </th>
                    <td>
                        {{ $user->name }}
                    </td>
                </tr>
                <tr>
                     <tr>
                    <th>
                        {{ trans('global.user.fields.gender') }}
                    </th>
                    <td>
                        {{ $user->gender }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.user.fields.phone') }}
                    </th>
                    <td>
                        {{ $user->phone }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.user.fields.address') }}
                    </th>
                    <td>
                        {{ $user->address }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.user.fields.current_address') }}
                    </th>
                    <td>
                        {{ $user->current_address }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.user.fields.image') }}
                    </th>
                    <td>
                        {{ $user->image }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.user.fields.facebook') }}
                    </th>
                    <td>
                        {{ $user->facebook }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.user.fields.status') }}
                    </th>
                    <td>
                        {{ $user->status }}
                    </td>
                </tr>
                    <th>
                        {{ trans('global.user.fields.email') }}
                    </th>
                    <td>
                        {{ $user->email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.email_verified_at') }}
                    </th>
                    <td>
                        {{ $user->email_verified_at }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Roles
                    </th>
                    <td>
                        @foreach($user->roles as $id => $roles)
                            <span class="label label-info label-many">{{ $roles->title }}</span>
                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection