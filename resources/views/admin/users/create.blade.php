@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.user.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.users.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('student_id')?'has-error' : '' }}">
                <div class="row">
                <div class="col-sm-4 " align="center">
               <label for="name">{{ trans('global.user.fields.student_id') }}*</label></div>
                   <div class="col-sm-6 " align="center"><input type="text" id="student_id" name="student_id" class="form-control" value="{{ old('student_id', isset($user) ? $user->student_id : '') }}">
                @if($errors->has('student_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('student_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.student_id_helper') }}
                </p>
            </div>
        </div>
      </div>
             
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                 <div class="row">
                   <div class="col-sm-4 " align="center">
                      <label for="name">{{ trans('global.user.fields.name') }}*</label></div>
                      <div class="col-sm-6 " align="center"> <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($user) ? $user->name : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.name_helper') }}
                </p>
            </div>
          </div>
        </div>
        
       

             <div class="form-group {{ $errors->has('gender')?'has-error' : '' }}">
                <div class="row">
                     <div class="col-sm-4 " align="center"> 
                       <label for="gender">{{ trans('global.user.fields.gender') }}*</label></div>
                     <div class="col-sm-6 " align="center">


                      <div class="radio">
           <label><input type="radio" name="gender" checked value="male">Male</label>
           <label><input type="radio" name="gender" value="Female">Female</label> 
          </div>
                @if($errors->has('gender'))
                    <em class="invalid-feedback">
                        {{ $errors->first('gender') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.gender_helper') }}
                </p></div></div>
            </div>
             <div class="form-group {{ $errors->has('phone')?'has-error' : '' }}">
                 <div class="row">
                    <div class="col-sm-4 " align="center">
                      <label for="name">{{ trans('global.user.fields.phone') }}*</label></div>
                         <div class="col-sm-6 " align="center"> <input type="text" id="phone" name="phone" class="form-control" value="{{ old('phone', isset($user) ? $user->phone : '') }}">
                @if($errors->has('phone'))
                    <em class="invalid-feedback">
                        {{ $errors->first('phone') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.phone_helper') }}
                </p>
            </div></div></div>
            <div class="form-group {{ $errors->has('address')?'has-error' : '' }}">
                <div class="row">
                <div class="col-sm-4 " align="center">
                    <label for="name">{{ trans('global.user.fields.address') }}*</label></div>
                  <div class="col-sm-6 " align="center"> <input type="text" id="address" name="address" class="form-control" value="{{ old('address', isset($user) ? $user->address : '') }}">
                @if($errors->has('address'))
                    <em class="invalid-feedback">
                        {{ $errors->first('address') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.address_helper') }}
                </p>
            </div></div></div>
            <div class="form-group {{ $errors->has('current_address')?'has-error' : '' }}">
                <div class="row">
                <div class="col-sm-4 " align="center">
                   <label for="name">{{ trans('global.user.fields.current_address') }}*</label></div>
                   <div class="col-sm-6 " align="center"><input type="text" id="current_address" name="current_address" class="form-control" value="{{ old('current_address', isset($user) ? $user->current_address : '') }}">
                @if($errors->has('current_address'))
                    <em class="invalid-feedback">
                        {{ $errors->first('current_address') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.current_address_helper') }}
                </p>
            </div></div></div>
             <div class="form-group {{ $errors->has('image')?'has-error' : '' }}">
                <div class="row">
                <div class="col-sm-4 " align="center">
                   <label for="name">{{ trans('global.user.fields.image') }}*</label></div>
                   <div class="col-sm-6 " align="center"><input type="file" id="image" name="image" class="form-control" value="{{ old('image', isset($user) ? $user->image : '') }}">
                @if($errors->has('image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.image_helper') }}
                </p>
            </div></div></div>
             <div class="form-group {{ $errors->has('facebook')?'has-error' : '' }}">
                <div class="row">
                  <div class="col-sm-4 " align="center">
                   <label for="name">{{ trans('global.user.fields.facebook') }}*</label></div>
                 <div class="col-sm-6 " align="center">  <input type="text" id="facebook" name="facebook" class="form-control" value="{{ old('facebook', isset($user) ? $user->facebook : '') }}">
                @if($errors->has('facebook'))
                    <em class="invalid-feedback">
                        {{ $errors->first('facebook') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.facebook_helper') }}
                </p>
            </div></div></div>
            <div class="form-group {{ $errors->has('kingqueenselected')?'has-error' : '' }}">
                <div class="row">
                     <div class="col-sm-4 " align="center"> 
                       <label for="kingqueenselected">{{ trans('global.user.fields.kingqueenselected') }}*</label></div>
                     <div class="col-sm-6 " align="center">


                      <div class="radio">
           <label><input type="radio" name="kingqueenselected"  value="yes">Yes</label>
           <label><input type="radio" name="kingqueenselected"  checked value="no">NO</label> 
          </div>
                @if($errors->has('kingqueenselected'))
                    <em class="invalid-feedback">
                        {{ $errors->first('kingqueenselected') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.kingqueenselected_helper') }}
                </p></div></div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                 <div class="row">
                   <div class="col-sm-4 " align="center">
                     <label for="email">{{ trans('global.user.fields.email') }}*</label></div>
               <div class="col-sm-6 " align="center">
                <input type="email" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}"> 
                @if($errors->has('email'))
                    <em class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.email_helper') }}
                </p>
            </div></div></div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <div class="row">
                  <div class="col-sm-4 " align="center">
                      <label for="password">{{ trans('global.user.fields.password') }}</label></div>
                  <div class="col-sm-6 " align="center"> <input type="password" id="password" name="password" class="form-control">
                @if($errors->has('password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.password_helper') }}
                </p>
            </div></div></div>
            <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                <div class="row">
                  <div class="col-sm-4 " align="center" >
                     <label for="roles">{{ trans('global.user.fields.roles') }}*
                    </label></div>
              <div class="col-sm-6 " align="center">  <select name="roles[]" id="roles" class="form-control select2" multiple="1">
                    @foreach($roles as $id => $roles)
                        <option value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($user) && $user->roles->contains($id)) ? 'selected' : '' }}>
                            {{ $roles }}
                        </option>
                    @endforeach
                </select>
                @if($errors->has('roles'))
                    <em class="invalid-feedback">
                        {{ $errors->first('roles') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.roles_helper') }}
                </p>
            </div></div></div>
            <div align="center">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}"  >
            </div>
        </form>
    </div>
</div>

@endsection