@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.user.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.kingqueen.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('roll') ? 'has-error' : '' }}">
            <div class="row">
             <div class="col-sm-4 " align="center">
               <label for="name">Academic year*</label></div>
               <div class="col-sm-4 " align="center">
        <select   class="form-control" id="sel1" name="year_id"  >
         
        @foreach($years as $key => $year)
                    <option value="{{ $year->id  }}">From {{$year->start_year}} To {{$year->end_year}}</option>  
                    @endforeach
      </select>
    </div>
    </div></div>
    <div class="form-group {{ $errors->has('roll') ? 'has-error' : '' }}"> 
         <div class="row">
             <div class="col-sm-4 " align="center">
               <label for="name">{{ trans('global.user.fields.name') }}*</label></div>
               <div class="col-sm-4 " align="center">
        <select   class="form-control" id="sel1" name="user_id"  >
         
        @foreach($users as $key => $user)
                    <option value="{{ $user->id  }}">{{$user->name}}</option>  
                    @endforeach
      </select></div>
    </div> 
  </div>
    <div class="form-group {{ $errors->has('roll') ? 'has-error' : '' }}">
      <div class="row">
             <div class="col-sm-4 " align="center">
               <label for="name">{{ trans('global.user.fields.status') }}*</label></div>
               <div class="col-sm-4 " align="center">
        <select   class="form-control" id="sel1" name="product_id"  >
          <option value="null">null</option> 
        @foreach($products as $key => $product)
                    <option value="{{ $product->id  }}">{{$product->name}}</option>  
                    @endforeach
      </select></div>
    </div> 
</div>
<div class="form-group {{ $errors->has('roll') ? 'has-error' : '' }}">
      <div class="row">
             <div class="col-sm-4 " align="center">
               <label for="name">Year*</label></div>
               <div class="col-sm-4 " align="center">
        <select   class="form-control" id="sel1" name="academic_id"  >
         
        
                    <option value="1">first year</option>  
                    <option value="2">second year</option> 
                    <option value="3">third year</option> 
                    <option value="4">fourth year</option> 
                    <option value="5">fifth year</option> 
      </select></div>
    </div> 
</div>
    <div class="form-group {{ $errors->has('roll') ? 'has-error' : '' }}">
                 <div class="row">
                   <div class="col-sm-4 " align="center">
                      <label for="roll">{{ trans('global.user.fields.roll') }}*</label></div>
                      <div class="col-sm-4" align="center"> <input type="text" id="roll" name="roll" class="form-control" value="{{ old('roll', isset($kingqueen) ? $kingqueen->roll : '') }}">
                @if($errors->has('roll'))
                    <em class="invalid-feedback">
                        {{ $errors->first('roll') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.roll_helper') }}
                </p>
            </div>
          </div>
        </div>

            <div align="center">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}"  >
            </div>
        </form>
    </div>
</div>

@endsection
 