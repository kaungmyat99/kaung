@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                 @foreach($kingqueeninfo as $key => $kingqueeninfo)
                <tr>
                    <th>
                        {{ trans('global.user.fields.student_id') }}
                    </th>
                    <td>
                       <img src=" {{ $kingqueeninfo->image ?? '' }}" alt="" width="80px" height="80px" >
                    </td>
                </tr>

                <tr>
                    <th>
                        {{ trans('global.user.fields.name') }}
                    </th>
                    <td>
                        {{ $kingqueeninfo->Name }}
                    </td>
                </tr>
                <tr>
                     <tr>
                    <th>
                        {{ trans('global.user.fields.gender') }}
                    </th>
                    <td>
                        {{ $kingqueeninfo->roll }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.user.fields.phone') }}
                    </th>
                    <td>
                        {{ $kingqueeninfo->name }}
                    </td>
                </tr>
                  @endforeach
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection