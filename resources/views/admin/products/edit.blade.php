@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.king_queen.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.products.update", [$product->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
              <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                     <div class="row">
                <div class="col-sm-4 " align="center">  <label for="name" >{{ trans('global.king_queen.fields.name') }}*</label></div>
                 <div class="col-sm-6" > <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($product) ? $product->name : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.king_queen.fields.name_helper') }}
                </p>
            </div>
        </div>
    </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                  <div class="row">
                <div class="col-sm-4 " align="center">
                <label for="description">{{ trans('global.king_queen.fields.description') }}</label></div>
                <div class="col-sm-6 " align="center"><textarea id="description" name="description" class="form-control ">{{ old('description', isset($product) ? $product->description : '') }}</textarea></div></div>
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.king_queen.fields.description_helper') }}
                </p>
            </div>
             <div class="form-group {{ $errors->has('status')?'has-error' : '' }}">
                <div class="row">
                     <div class="col-sm-4 " align="center"> 
                       <label for="status">{{ trans('global.user.fields.status') }}*</label></div>
                     <div class="col-sm-6 " align="center">  <div class="radio">
           <label><input type="radio" name="status"    value="yes"  {{ ($product->status=='yes')? 'checked' : '' }}>Yes</label>
           <label><input type="radio" name="status" value="no" {{ ($product->status=='no')? 'checked' : '' }} >No</label> 
          
       
       </div>
                @if($errors->has('status'))
                    <em class="invalid-feedback">
                        {{ $errors->first('status') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.user.fields.status_helper') }}
                </p></div></div>
            </div>
            <div align="center">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection