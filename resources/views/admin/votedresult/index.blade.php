 @extends('layouts.admin')
@section('content')
   <div class="card">
    <div class="card-header">
        <form  action="{{ route("admin.votedresult.index") }}" method="get"> 
               <div class="row">
                 <div class="col-sm-1" align="center">
               <label for="name">Academic*</label></div>
              <div class="col-sm-3" align="center" >
       <select   class="form-control" id="sel1" name="academic_year"  >
         
        @foreach($academic_years as $key => $academic_year)
                    <option value="{{ $academic_year->id  }}" {{($academic_year->id==$academic_yearselected)?'selected':''}}>from{{$academic_year->start_year}} To {{$academic_year->end_year}} </option>  
                    @endforeach
      </select>  
    </div>
                
         
             <div class="col-sm-1" align="center">
               <label for="name">Type*</label></div>
            <div class="col-sm-3" align="center" >
       <select   class="form-control" id="sel1" name="product"  >
         
        @foreach($products as $key => $product)
                    <option value="{{ $product->id  }}" {{($product->id ==$selectedproduct)?'selected':''}}>{{$product->name}}</option>  
                    @endforeach
      </select>  
    </div>   
     
 <div class="col-sm-2" align="center">
   <button  type="submit" class="btn btn-success">serach</button>
 </div>
 <div class="col-sm-2" align="center">
  <a class="btn btn-success"  href="{{ route("admin.votedresult.show",$selectedproduct) }}">Result list</a>
 </div>
</div>
</form>
 </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
               
                <tbody>
                  
                             <div class="row">
                               @foreach($kingqueens as $key => $kingqueen)
                               <form  action="{{ route("frontend.kingqueen.store") }}" method="POST" enctype="multipart/form-data">
                               @csrf
                               <div class="col-md-3">
                                
                            <div align="center" data-entry-id="{{ $kingqueen->id }}">
                                  <input type="hidden" name="id" value="{{ $kingqueen->id }}">
                                  <input type="hidden" name="kingqueen_id" value="{{ $kingqueen->ID }}">
                                  <img class="masthead-avatar mb-5" src=" {{ $kingqueen->image ?? '' }}" alt="" width="200px" height="200px" > <br>
                                  <span>{{ $kingqueen->name }} </span><br>
                                   <span>{{ $kingqueen->count }} </span><br>
                                 
                                     
                             
                          </div>
                        </div> 
                      </form>
                            @endforeach
                         
                            </div>

                         
                    
                </tbody>
            </table>
        </div>
    </div>

</div>
 
      
@endsection