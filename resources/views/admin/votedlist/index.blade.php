@extends('layouts.admin')
@section('content')
@can('product_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.votedlist.create') }}">
               Calculate Final Voting Result
            </a>
        </div>
    </div>
@endcan

<div class="card">

    <div class="card-header" >
 <form  action="{{ route("admin.votedlist.index") }}" method="get"> 
               <div class="row">
                 <div class="col-sm-1" align="center">
               <label for="name">Academic*</label></div>
              <div class="col-sm-3" align="center" >
       <select   class="form-control" id="sel1" name="academic_year"  >
         
        @foreach($academic_years as $key => $academic_year)
                    <option value="{{ $academic_year->id  }}" {{($academic_year->id==$academic_yearselected)?'selected':''}}>from{{$academic_year->start_year}} To {{$academic_year->end_year}} </option>  
                    @endforeach
      </select>  
    </div>
                
         
             <div class="col-sm-1" align="center">
               <label for="name">Type*</label></div>
              <div class="col-sm-3" align="center" >
       <select   class="form-control" id="sel1" name="product"  >
         
        @foreach($products as $key => $product)
                    <option value="{{ $product->id  }}" {{($product->id ==$selectedproduct)?'selected':''}}>{{$product->name}}</option>  
                    @endforeach
      </select>  
    </div>   
     
 <div class="col-sm-2" align="center">
   <button  type="submit" class="btn btn-success">serach</button>
 </div>
 <div class="col-sm-2" align="center">
  <a class="btn btn-success"  href="{{ route("admin.votedlist.show",$selectedproduct) }}">Voted List Delete</a>
 </div>
</div>
</form>
        <!--{{ trans('global.king_queen.title_singular') }} {{ trans('global.list') }}-->
    </div>


    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                             User
                        </th>
                        <th>
                            Voted 
                        </th>
                        <th>
                           Type
                        </th>
                         
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                    
                        <tr data-entry-id="{{ $user->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $user->user_name ?? '' }}
                            </td>
                             <td>
                                {{ $user->kingqueen ?? '' }}
                            </td>
                             <td>
                                {{ $user->type ?? '' }}
                            </td>

                        </tr>
                     
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
 
@endsection 